package exo2;

public class Artist {
    String firstname;
    String lastname;

    String getFullName() {
        return firstname + ' ' + lastname;
    }

    public Artist(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public static void main(String[] args) {
        Artist artist = new Artist("anthony", "berot");
        System.out.println(artist.getFullName());
    }
}
