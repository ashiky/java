package exo2;

import java.util.ArrayList;

public class Playlist {
    int currentMusicIndex;
    ArrayList<Music> musicList;

    public Playlist(ArrayList<Music> musicList) {
        this.currentMusicIndex = 0;
        this.musicList = musicList;
    }

    void addMusic(Music newMusic) {
        this.musicList.add(newMusic);
    }

    void setCurrentMusic(int i) {
        this.currentMusicIndex = i;
    }

    int getCurrentMusicIndex() {
        return this.currentMusicIndex;
    }

    String getCurrentMusic() {
        return this.musicList.get(this.currentMusicIndex).title;
    }

    void next() {
        if (musicList.size() > this.getCurrentMusicIndex()) {
            this.setCurrentMusic(this.currentMusicIndex + 1);
        } else {
            this.setCurrentMusic(0);
        }
    }

    void removeMusic(int index) {
        this.musicList.remove(index);
    }

    String getTotalDuration() {
        int totalDuration = 0;
        for (int i = 0; i < musicList.size(); i++) {
            totalDuration += musicList.get(i).duration;
        }
        return (totalDuration / 60) + "m" + (totalDuration % 60) + "s";
    }
}
