package exo2;

import java.util.ArrayList;
import java.util.Arrays;

public class Player {
    public static void main(String[] args) {
        Artist artist = new Artist("anthony", "berot");
        System.out.println(artist.getFullName());

        ArrayList<Artist> artistArray = new ArrayList<Artist>();
        artistArray.add(artist);

        Music deadUnicorn = new Music("dead unicorn", 433, artistArray);
        System.out.println(deadUnicorn.getInfos());

        ArrayList<Music> musicArray = new ArrayList<Music>();
        musicArray.add(deadUnicorn);

        Playlist fav = new Playlist(musicArray);
        Music vendetta = new Music("vendetta", 433, artistArray);
        fav.addMusic(vendetta);
        System.out.println(fav.getTotalDuration());
        System.out.println(fav.getCurrentMusic());
        fav.next();
        System.out.println(fav.getCurrentMusic());
        fav.removeMusic(0);
        System.out.println(fav.getTotalDuration());
    }
}