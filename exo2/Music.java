package exo2;

import java.util.ArrayList;
import java.util.Arrays;

public class Music {
    String title;
    int duration;
    ArrayList<Artist> artistSet;

    public Music(String title, int duration, ArrayList<Artist> artistSet) {
        this.title = title;
        this.duration = duration;
        this.artistSet = artistSet;
    }

    String getDuration() {
        return (duration / 60) + "m" + (duration % 60) + "s";
    }

    String getArtist() {
        var artist = "";
        for (int i = 0;i < this.artistSet.size(); i ++) {
            artist = artist + this.artistSet.get(i).firstname + " " + this.artistSet.get(i).lastname + " " ; 
        }
        return artist;
    }

    String getInfos() {
        return  getArtist() + ' ' + title + ' ' + getDuration();
    }
}
