public class Cinema {
    public static void main(String[] args) {
        String[]  employeeNames = {"Alice Dupont", "Bob Martin", "Charlie Besson", "Diane Loriot", "Eva Joly"}; 
        int[] hoursWorked = {35, 38, 35, 38, 40};
        double[] hourlyRates = {12.5, 15.0, 13.5, 14.5, 13.0};
        String[] positions = {"Caissier", "Projectionniste", "Caissier", "Manager", "Caissier"};

        String searchPosition = "Caissier";

        for(int i = 0; i < employeeNames.length; i++) {
            double pay;
            if(hoursWorked[i] < 35) {
                double supHours = hoursWorked[i] - 35;
                pay = 35 * hourlyRates[i] + supHours * (1.5 * hourlyRates[i]);
            } else {
                pay = hoursWorked[i] * hourlyRates[i]; 
            }
            System.out.println(pay);
        };

        for(int i = 0; i < employeeNames.length; i++) {
            if(positions[i] == searchPosition) {
                System.out.println(employeeNames[i]);
            }
        };
    }
  }